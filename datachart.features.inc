<?php
/**
 * @file
 * datachart.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function datachart_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function datachart_node_info() {
  $items = array(
    'chart' => array(
      'name' => t('Chart'),
      'base' => 'node_content',
      'description' => t('Choosen data source and file name, this content draws the chart named file name. '),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'data' => array(
      'name' => t('Data'),
      'base' => 'node_content',
      'description' => t('Stores updated data for feeding charts. '),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => t('This content is updated automatically. Don\'t edit it by hand unless it is extremely necessary.'),
    ),
  );
  return $items;
}
